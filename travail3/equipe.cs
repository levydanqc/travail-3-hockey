﻿/*******************************************************************************
 * Fichier : equipe.cs
 * 
 * Classe : Equipe
 * 
 * Description : Classe représentant une équipe de hockey.
 * 
 * Auteur : Dan Lévy
 * 
 * GitLab : https://gitlab.com/levydanqc/travail-3-hockey
*******************************************************************************/

using System;
using System.Collections.Generic;

namespace Hockey
{
    public class Equipe
    {
        private string m_strCode;
        private string m_strNom;
        private string m_strEndroit;
        private List<Joueur> m_lesJoueurs;

        public Equipe(string strCode, string strNom, string strEndroit)
        {
            if (strCode == "" || strCode == null)
            {
                throw new ArgumentNullException(null, "Le code est nul.");
            }
            if (strCode.Length != 3)
            {
                throw new ArgumentException(
                    "La longueur du code est invalide.");
            }
            foreach (char charCode in strCode)
            {
                if (!(charCode >= 'A' && charCode <= 'Z') &&
                    !(charCode >= 'a' && charCode <= 'z'))
                {
                    throw new ArgumentException
                        ("Le code contient au moins un caractère invalide.");
                }
            }
            m_strCode = strCode.ToUpper();

            if (strNom == "" || strNom == null)
            {
                throw new ArgumentNullException(null, "Le nom est nul.");
            }
            if (strNom.Length < 1 || strNom.Length > 32)
            {
                throw new ArgumentException("La longueur du nom est invalide.");
            }
            m_strNom = strNom;

            if (strEndroit == "" || strEndroit == null)
            {
                throw new ArgumentNullException(null, "L'endroit est nul.");
            }
            if (strEndroit.Length < 1 || strEndroit.Length > 32)
            {
                throw new ArgumentException(
                    "La longueur de l'endroit est invalide.");
            }
            m_strEndroit = strEndroit;
            m_lesJoueurs = new List<Joueur>();
        }

        public string Code
        {
            get { return m_strCode; }
        }

        public string Nom
        {
            get { return m_strNom; }
        }

        public string Endroit
        {
            get { return m_strEndroit; }
        }

        public override string ToString()
        {
            return String.Format("{0} de {1}", m_strNom, m_strEndroit);
        }

        public static bool operator ==(in Equipe equipe1,
            in Equipe equipe2)
        {
            if (Object.ReferenceEquals(equipe1, equipe2))
            {
                return true;
            }
            if ((Object)equipe1 == null
               || (Object)equipe2 == null)
            {
                return false;
            }
            return (equipe1.m_strCode == equipe2.m_strCode);
        }

        public static bool operator !=(in Equipe equipe1,
            in Equipe equipe2)
        {
            return !(equipe1 == equipe2);
        }

        public override bool Equals(object obj)
        {
            if (obj is Equipe)
            {
                return (this == (Equipe)obj);
            }
            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public Joueur ObtenirJoueur(int iIndice)
        {
            if (!(iIndice >= 0 && iIndice < m_lesJoueurs.Count))
            {
                return null;
            }
            return m_lesJoueurs[iIndice];
        }

        public bool Ajouter(in Joueur leJoueur)
        {
            if (!m_lesJoueurs.Contains(leJoueur) && leJoueur != null)
            {
                m_lesJoueurs.Add(leJoueur);
                return true;
            }
            return false;
        }

        public bool Retirer(in Joueur leJoueur)
        {
            return m_lesJoueurs.Remove(leJoueur);
        }
    }
}
