﻿/*******************************************************************************
 * Fichier : joueur.cs
 * 
 * Classe : Joueur
 * 
 * Description : Classe représentant un joueur de hockey.
 * 
 * Auteur : Dan Lévy
 * 
 * GitLab : https://gitlab.com/levydanqc/travail-3-hockey
*******************************************************************************/

using System;
namespace Hockey
{
    public enum Position
    {
        Centre,
        AilierGauche,
        AilierDroit,
        Defenseur,
        GardienDeBut,
        ADeterminer
    }

    public class Joueur
    {
        private string m_strNom;
        private string m_strPrenom;
        private Position m_laPostion;

        public Joueur(string strNom, string strPrenom, Position laPostion)
        {
            this.Nom = strNom;
            this.Prenom = strPrenom;
            this.Position = laPostion;
        }

        public string Nom
        {
            get { return this.m_strNom; }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException(null, "Le nom est nul.");
                }
                if (value.Length < 1 || value.Length > 32)
                {
                    throw new ArgumentException(
                        "La longueur du nom est invalide.");
                }
                m_strNom = value;
            }
        }

        public string Prenom
        {
            get { return this.m_strPrenom; }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException(
                        null, "Le prénom est nul.");
                }
                if (value.Length < 1 || value.Length > 32)
                {
                    throw new ArgumentException(
                        "La longueur du prénom est invalide.");
                }
                m_strPrenom = value;
            }
        }

        public Position Position
        {
            get { return m_laPostion; }
            set
            {
                if (!Enum.IsDefined(typeof(Position), value))
                {
                    throw new ArgumentException("La position est invalide.");
                }

                m_laPostion = value;
            }
        }

        public override string ToString()
        {
            char cPos;
            switch (m_laPostion)
            {
                case Position.Centre:
                    cPos = 'C';
                    break;
                case Position.AilierGauche:
                    cPos = 'L';
                    break;
                case Position.AilierDroit:
                    cPos = 'R';
                    break;
                case Position.Defenseur:
                    cPos = 'D';
                    break;
                case Position.GardienDeBut:
                    cPos = 'G';
                    break;
                case Position.ADeterminer:
                    cPos = 'I';
                    break;
                default:
                    cPos = ' ';
                    break;
            }
            return String.Format(
                "{0, -69}({1})",
                m_strNom + ", " + m_strPrenom,
                cPos
                );
        }
    }
}