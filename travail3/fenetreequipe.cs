﻿/*******************************************************************************
 * Fichier : fenetreequipe.cs
 * 
 * Classe : FenetreEquipe
 * 
 * Description : Fenêtre d'ajout d'une équipe de hockey.
 * 
 * Auteur : Dan Lévy
 * 
 * GitLab : https://gitlab.com/levydanqc/travail-3-hockey
*******************************************************************************/

using System;
using System.Windows.Forms;
using Hockey;

namespace travail3
{
    public partial class FenetreEquipe : Form
    {
        private Equipe m_laEquipe;

        public FenetreEquipe ()
        {
            InitializeComponent ();
            FormBorderStyle = FormBorderStyle.FixedDialog;
            MinimizeBox = false;
            MaximizeBox = false;
            ShowInTaskbar = false;
            AcceptButton = m_btnOK;
            CancelButton = m_btnAnnuler;
            m_btnOK.DialogResult = DialogResult.OK;
            m_btnAnnuler.DialogResult = DialogResult.Cancel;
        }

        public Equipe LaEquipe
        {
            get { return m_laEquipe; }
        }

        private void m_btnOK_Click (object sender, EventArgs e)
        {
            try
            {
                m_laEquipe = new Equipe (
                    m_txtCode.Text, m_txtNom.Text, m_txtEndroit.Text);
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show (ex.Message,
                    FenetrePrincipale.TITRE_APPLICATION, MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
                DialogResult = DialogResult.None;
            }
        }
    }
}
