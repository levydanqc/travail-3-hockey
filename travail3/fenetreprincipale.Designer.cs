﻿namespace travail3
{
    partial class FenetrePrincipale
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose (bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose ();
            }
            base.Dispose (disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent ()
        {
            this.m_lblEquipes = new System.Windows.Forms.Label();
            this.m_lstEquipes = new System.Windows.Forms.ListBox();
            this.m_lblAgentsLibres = new System.Windows.Forms.Label();
            this.m_lstAgentsLibres = new System.Windows.Forms.ListBox();
            this.m_btnAjouterEquipe = new System.Windows.Forms.Button();
            this.m_btnSupprimerEquipe = new System.Windows.Forms.Button();
            this.m_btnAjouterAgentLibre = new System.Windows.Forms.Button();
            this.m_btnRecruterAgentLibre = new System.Windows.Forms.Button();
            this.m_btnAPropos = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // m_lblEquipes
            // 
            this.m_lblEquipes.AutoSize = true;
            this.m_lblEquipes.Location = new System.Drawing.Point(12, 9);
            this.m_lblEquipes.Name = "m_lblEquipes";
            this.m_lblEquipes.Size = new System.Drawing.Size(48, 13);
            this.m_lblEquipes.TabIndex = 0;
            this.m_lblEquipes.Text = "É&quipes:";
            // 
            // m_lstEquipes
            // 
            this.m_lstEquipes.FormattingEnabled = true;
            this.m_lstEquipes.Location = new System.Drawing.Point(12, 25);
            this.m_lstEquipes.Name = "m_lstEquipes";
            this.m_lstEquipes.Size = new System.Drawing.Size(256, 264);
            this.m_lstEquipes.TabIndex = 1;
            // 
            // m_lblAgentsLibres
            // 
            this.m_lblAgentsLibres.AutoSize = true;
            this.m_lblAgentsLibres.Location = new System.Drawing.Point(274, 9);
            this.m_lblAgentsLibres.Name = "m_lblAgentsLibres";
            this.m_lblAgentsLibres.Size = new System.Drawing.Size(70, 13);
            this.m_lblAgentsLibres.TabIndex = 2;
            this.m_lblAgentsLibres.Text = "&Agents libres:";
            // 
            // m_lstAgentsLibres
            // 
            this.m_lstAgentsLibres.FormattingEnabled = true;
            this.m_lstAgentsLibres.Location = new System.Drawing.Point(277, 25);
            this.m_lstAgentsLibres.Name = "m_lstAgentsLibres";
            this.m_lstAgentsLibres.Size = new System.Drawing.Size(544, 264);
            this.m_lstAgentsLibres.TabIndex = 3;
            // 
            // m_btnAjouterEquipe
            // 
            this.m_btnAjouterEquipe.Location = new System.Drawing.Point(827, 25);
            this.m_btnAjouterEquipe.Name = "m_btnAjouterEquipe";
            this.m_btnAjouterEquipe.Size = new System.Drawing.Size(145, 23);
            this.m_btnAjouterEquipe.TabIndex = 4;
            this.m_btnAjouterEquipe.Text = "A&jouter une équipe";
            this.m_btnAjouterEquipe.UseVisualStyleBackColor = true;
            this.m_btnAjouterEquipe.Click += new System.EventHandler(this.m_btnAjouterEquipe_Click);
            // 
            // m_btnSupprimerEquipe
            // 
            this.m_btnSupprimerEquipe.Location = new System.Drawing.Point(827, 54);
            this.m_btnSupprimerEquipe.Name = "m_btnSupprimerEquipe";
            this.m_btnSupprimerEquipe.Size = new System.Drawing.Size(145, 23);
            this.m_btnSupprimerEquipe.TabIndex = 5;
            this.m_btnSupprimerEquipe.Text = "&Supprimer une équipe";
            this.m_btnSupprimerEquipe.UseVisualStyleBackColor = true;
            this.m_btnSupprimerEquipe.Click += new System.EventHandler(this.m_btnSupprimerEquipe_Click);
            // 
            // m_btnAjouterAgentLibre
            // 
            this.m_btnAjouterAgentLibre.Location = new System.Drawing.Point(827, 83);
            this.m_btnAjouterAgentLibre.Name = "m_btnAjouterAgentLibre";
            this.m_btnAjouterAgentLibre.Size = new System.Drawing.Size(145, 23);
            this.m_btnAjouterAgentLibre.TabIndex = 6;
            this.m_btnAjouterAgentLibre.Text = "Aj&outer un agent libre";
            this.m_btnAjouterAgentLibre.UseVisualStyleBackColor = true;
            this.m_btnAjouterAgentLibre.Click += new System.EventHandler(this.m_btnAjouterAgentLibre_Click);
            // 
            // m_btnRecruterAgentLibre
            // 
            this.m_btnRecruterAgentLibre.Location = new System.Drawing.Point(827, 112);
            this.m_btnRecruterAgentLibre.Name = "m_btnRecruterAgentLibre";
            this.m_btnRecruterAgentLibre.Size = new System.Drawing.Size(145, 23);
            this.m_btnRecruterAgentLibre.TabIndex = 7;
            this.m_btnRecruterAgentLibre.Text = "&Recruter un agent libre";
            this.m_btnRecruterAgentLibre.UseVisualStyleBackColor = true;
            this.m_btnRecruterAgentLibre.Click += new System.EventHandler(this.m_btnRecruterAgentLibre_Click);
            // 
            // m_btnAPropos
            // 
            this.m_btnAPropos.Location = new System.Drawing.Point(827, 141);
            this.m_btnAPropos.Name = "m_btnAPropos";
            this.m_btnAPropos.Size = new System.Drawing.Size(145, 23);
            this.m_btnAPropos.TabIndex = 8;
            this.m_btnAPropos.Text = "À &propos de";
            this.m_btnAPropos.UseVisualStyleBackColor = true;
            this.m_btnAPropos.Click += new System.EventHandler(this.m_btnAPropos_Click);
            // 
            // FenetrePrincipale
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(986, 304);
            this.Controls.Add(this.m_btnAPropos);
            this.Controls.Add(this.m_btnRecruterAgentLibre);
            this.Controls.Add(this.m_btnAjouterAgentLibre);
            this.Controls.Add(this.m_btnSupprimerEquipe);
            this.Controls.Add(this.m_btnAjouterEquipe);
            this.Controls.Add(this.m_lstAgentsLibres);
            this.Controls.Add(this.m_lblAgentsLibres);
            this.Controls.Add(this.m_lstEquipes);
            this.Controls.Add(this.m_lblEquipes);
            this.Name = "FenetrePrincipale";
            this.Text = "fenetreprincipale";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FenetrePrincipale_FormClosed);
            this.Load += new System.EventHandler(this.FenetrePrincipale_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label m_lblEquipes;
        private System.Windows.Forms.ListBox m_lstEquipes;
        private System.Windows.Forms.Label m_lblAgentsLibres;
        private System.Windows.Forms.ListBox m_lstAgentsLibres;
        private System.Windows.Forms.Button m_btnAjouterEquipe;
        private System.Windows.Forms.Button m_btnSupprimerEquipe;
        private System.Windows.Forms.Button m_btnAjouterAgentLibre;
        private System.Windows.Forms.Button m_btnRecruterAgentLibre;
        private System.Windows.Forms.Button m_btnAPropos;
    }
}