﻿namespace travail3
{
    partial class FenetreEquipe
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose (bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose ();
            }
            base.Dispose (disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent ()
        {
            this.m_lblCode = new System.Windows.Forms.Label();
            this.m_txtCode = new System.Windows.Forms.TextBox();
            this.m_lblNom = new System.Windows.Forms.Label();
            this.m_txtNom = new System.Windows.Forms.TextBox();
            this.m_lblEndroit = new System.Windows.Forms.Label();
            this.m_txtEndroit = new System.Windows.Forms.TextBox();
            this.m_btnOK = new System.Windows.Forms.Button();
            this.m_btnAnnuler = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // m_lblCode
            // 
            this.m_lblCode.AutoSize = true;
            this.m_lblCode.Location = new System.Drawing.Point(12, 9);
            this.m_lblCode.Name = "m_lblCode";
            this.m_lblCode.Size = new System.Drawing.Size(35, 13);
            this.m_lblCode.TabIndex = 0;
            this.m_lblCode.Text = "&Code:";
            // 
            // m_txtCode
            // 
            this.m_txtCode.Location = new System.Drawing.Point(12, 25);
            this.m_txtCode.Name = "m_txtCode";
            this.m_txtCode.Size = new System.Drawing.Size(100, 20);
            this.m_txtCode.TabIndex = 1;
            // 
            // m_lblNom
            // 
            this.m_lblNom.AutoSize = true;
            this.m_lblNom.Location = new System.Drawing.Point(12, 59);
            this.m_lblNom.Name = "m_lblNom";
            this.m_lblNom.Size = new System.Drawing.Size(32, 13);
            this.m_lblNom.TabIndex = 2;
            this.m_lblNom.Text = "&Nom:";
            // 
            // m_txtNom
            // 
            this.m_txtNom.Location = new System.Drawing.Point(12, 75);
            this.m_txtNom.Name = "m_txtNom";
            this.m_txtNom.Size = new System.Drawing.Size(212, 20);
            this.m_txtNom.TabIndex = 3;
            // 
            // m_lblEndroit
            // 
            this.m_lblEndroit.AutoSize = true;
            this.m_lblEndroit.Location = new System.Drawing.Point(12, 111);
            this.m_lblEndroit.Name = "m_lblEndroit";
            this.m_lblEndroit.Size = new System.Drawing.Size(43, 13);
            this.m_lblEndroit.TabIndex = 4;
            this.m_lblEndroit.Text = "&Endroit:";
            // 
            // m_txtEndroit
            // 
            this.m_txtEndroit.Location = new System.Drawing.Point(12, 127);
            this.m_txtEndroit.Name = "m_txtEndroit";
            this.m_txtEndroit.Size = new System.Drawing.Size(212, 20);
            this.m_txtEndroit.TabIndex = 5;
            // 
            // m_btnOK
            // 
            this.m_btnOK.Location = new System.Drawing.Point(249, 25);
            this.m_btnOK.Name = "m_btnOK";
            this.m_btnOK.Size = new System.Drawing.Size(75, 23);
            this.m_btnOK.TabIndex = 6;
            this.m_btnOK.Text = "&OK";
            this.m_btnOK.UseVisualStyleBackColor = true;
            this.m_btnOK.Click += new System.EventHandler(this.m_btnOK_Click);
            // 
            // m_btnAnnuler
            // 
            this.m_btnAnnuler.Location = new System.Drawing.Point(249, 59);
            this.m_btnAnnuler.Name = "m_btnAnnuler";
            this.m_btnAnnuler.Size = new System.Drawing.Size(75, 23);
            this.m_btnAnnuler.TabIndex = 7;
            this.m_btnAnnuler.Text = "&Annuler";
            this.m_btnAnnuler.UseVisualStyleBackColor = true;
            // 
            // FenetreEquipe
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(339, 159);
            this.Controls.Add(this.m_btnAnnuler);
            this.Controls.Add(this.m_btnOK);
            this.Controls.Add(this.m_txtEndroit);
            this.Controls.Add(this.m_lblEndroit);
            this.Controls.Add(this.m_txtNom);
            this.Controls.Add(this.m_lblNom);
            this.Controls.Add(this.m_txtCode);
            this.Controls.Add(this.m_lblCode);
            this.Name = "FenetreEquipe";
            this.Text = "fenetreequipe";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label m_lblCode;
        private System.Windows.Forms.TextBox m_txtCode;
        private System.Windows.Forms.Label m_lblNom;
        private System.Windows.Forms.TextBox m_txtNom;
        private System.Windows.Forms.Label m_lblEndroit;
        private System.Windows.Forms.TextBox m_txtEndroit;
        private System.Windows.Forms.Button m_btnOK;
        private System.Windows.Forms.Button m_btnAnnuler;
    }
}