﻿/*******************************************************************************
 * Fichier : fenetreprincipale.cs
 * 
 * Classe : FenetrePrincipale
 * 
 * Description : Fenêtre principale du programme. Permet d'effecteur des
 * modifications sur la base de données.
 * 
 * Auteur : Dan Lévy
 * 
 * GitLab : https://gitlab.com/levydanqc/travail-3-hockey
*******************************************************************************/

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using Hockey;

namespace travail3
{
    public partial class FenetrePrincipale : Form
    {
        public const string TITRE_APPLICATION = "Travail 3";

        public FenetrePrincipale()
        {
            InitializeComponent();
            FormBorderStyle = FormBorderStyle.FixedSingle;
            MaximizeBox = false;
            Text = TITRE_APPLICATION;
            m_lstEquipes.Font = new Font("Lucida Console", 9);
            m_lstEquipes.Sorted = true;
            m_lstAgentsLibres.Font = new Font("Lucida Console", 9);
            m_lstAgentsLibres.Sorted = true;
        }

        private void FenetrePrincipale_Load(object sender, EventArgs e)
        {
            // Obtention des listes pour les équipes et les agents libres
            List<Equipe> lesEquipes = GestionBDLigue.ObtenirEquipes();
            List<Joueur> lesAgentsLibres = GestionBDLigue.ObtenirAgentsLibres();

            // Ajout des équipes à la liste des équipes
            for (int i = 0; i < lesEquipes.Count; i++)
            {
                m_lstEquipes.Items.Add(lesEquipes[i]);
            }

            // Ajout des agents libres à la liste des agents libres
            for (int i = 0; i < lesAgentsLibres.Count; i++)
            {
                m_lstAgentsLibres.Items.Add(lesAgentsLibres[i]);
            }

            // Sélection du 1er élément de chaque liste si elles contiennent
            // des valeurs
            if (m_lstEquipes.Items.Count > 0)
            {
                m_lstEquipes.SelectedIndex = 0;
            }
            if (m_lstAgentsLibres.Items.Count > 0)
            {
                m_lstAgentsLibres.SelectedIndex = 0;
            }

            // Préparation des requêtes
            GestionBDLigue.PreparerRequetes();
        }

        private void FenetrePrincipale_FormClosed(object sender,
            FormClosedEventArgs e)
        {
            GestionBDLigue.LibererRequetes();
        }

        private void m_btnAjouterEquipe_Click(object sender, EventArgs e)
        {
            FenetreEquipe frmAjoutEquipe;
            int iIndice;
            frmAjoutEquipe = new FenetreEquipe();

            frmAjoutEquipe.Text = TITRE_APPLICATION;

            if (frmAjoutEquipe.ShowDialog() == DialogResult.OK)
            {
                Equipe uneEquipe = frmAjoutEquipe.LaEquipe;

                if (m_lstEquipes.Items.Contains(uneEquipe))
                {
                    MessageBox.Show
                        ("Une équipe avec le même nom est déjà dans la liste.",
                        TITRE_APPLICATION,
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Information);
                }
                else
                { // FIXME
                    // Ajout à la base de donnée
                    GestionBDLigue.AjouterEquipe(uneEquipe);

                    // Ajout à la liste des équipes
                    iIndice = m_lstEquipes.Items.Add(uneEquipe);
                    m_lstEquipes.SelectedIndex = iIndice;
                }
            }

            frmAjoutEquipe.Dispose();
        }

        private void m_btnSupprimerEquipe_Click(object sender, EventArgs e)
        {
            Equipe uneEquipe;
            int iIndex;
            iIndex = m_lstEquipes.SelectedIndex;
            uneEquipe = (Equipe)m_lstEquipes.SelectedItem;

            if (uneEquipe == null) // Aucune équipe sélectionnée
            {
                MessageBox.Show(
                    "Vous devez sélectionner une équipe pour pouvoir la" +
                    " supprimer.",
                    TITRE_APPLICATION,
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
            }
            else
            {
                // Retrait de l'équipe de la base de données
                GestionBDLigue.SupprimerEquipe(uneEquipe);

                // Transfert des joueurs de l'équipe en tant qu'agents libres
                Joueur unJoueur = uneEquipe.ObtenirJoueur(0);
                while (unJoueur != null)
                {
                    uneEquipe.Retirer(unJoueur);
                    m_lstAgentsLibres.Items.Add(unJoueur);
                    unJoueur = uneEquipe.ObtenirJoueur(0);
                }

                // Retrait de l'équipe de la liste et sélection d'un élément
                // de la liste
                m_lstEquipes.Items.RemoveAt(iIndex);
                m_lstEquipes.SelectedIndex = iIndex -
                    Convert.ToInt32(iIndex == m_lstEquipes.Items.Count);
            }
        }

        private void m_btnAjouterAgentLibre_Click(object sender, EventArgs e)
        {
            FenetreJoueur frmAjoutAgentLibre;
            int iIndice;
            frmAjoutAgentLibre = new FenetreJoueur();

            frmAjoutAgentLibre.Text = TITRE_APPLICATION;

            if (frmAjoutAgentLibre.ShowDialog() == DialogResult.OK)
            {
                Joueur unJoueur = frmAjoutAgentLibre.LeJoueur;

                // Ajout à la base de donnée
                GestionBDLigue.AjouterJoueur(unJoueur);

                // Ajout à la liste des équipes
                iIndice = m_lstAgentsLibres.Items.Add(unJoueur);
                m_lstAgentsLibres.SelectedIndex = iIndice;
            }

            frmAjoutAgentLibre.Dispose();
        }

        private void m_btnRecruterAgentLibre_Click(object sender, EventArgs e)
        {
            Joueur unJoueur;
            Equipe uneEquipe;
            int iIndexJ;
            int iIndexE;
            iIndexE = m_lstEquipes.SelectedIndex;
            iIndexJ = m_lstAgentsLibres.SelectedIndex;
            uneEquipe = (Equipe)m_lstEquipes.SelectedItem;
            unJoueur = (Joueur)m_lstAgentsLibres.SelectedItem;

            if (uneEquipe == null) // Aucune équipe sélectionnée
            {
                MessageBox.Show(
                    "Vous devez sélectionner une équipe pour qu'elle puisse " +
                    "recruter un agent libre.",
                    TITRE_APPLICATION,
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
            }
            else if (unJoueur == null) // Aucun joueur sélectionné
            {
                MessageBox.Show(
                    "Vous devez sélectionner un agent libre pour qu'il " +
                    "puisse être recruté par une équipe.",
                    TITRE_APPLICATION,
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);

            }
            else
            {
                // Retrait de l'équipe de la base de données
                GestionBDLigue.AffecterJoueurAEquipe(uneEquipe, unJoueur);

                // Ajout du joueur à l'équipe
                uneEquipe.Ajouter(unJoueur);

                // Retrait de l'agent libre et sélection d'un agent libre
                m_lstAgentsLibres.Items.RemoveAt(iIndexJ);
                m_lstAgentsLibres.SelectedIndex = iIndexJ -
                    Convert.ToInt32(iIndexJ == m_lstAgentsLibres.Items.Count);
            }
        }

        private void m_btnAPropos_Click(object sender, EventArgs e)
        {
            MessageBox.Show(
                String.Format(
                    "{0} réalisé par {1} ({2}).", TITRE_APPLICATION,
                    "Dan Lévy", "1861154"),
                TITRE_APPLICATION,
                MessageBoxButtons.OK,
                MessageBoxIcon.Information);
        }
    }
}
