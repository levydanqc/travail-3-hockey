﻿/*******************************************************************************
 * Fichier : gestionbdligue.cs
 * 
 * Classe : GestionBdLigue
 * 
 * Description : Classe statique de gestion de la connexion et des commandes
 * liées à la base de données.
 * 
 * Auteur : Dan Lévy
 * 
 * GitLab : https://gitlab.com/levydanqc/travail-3-hockey
*******************************************************************************/

using System.Collections.Generic;
using MySqlConnector;

namespace Hockey
{
    public static class GestionBDLigue
    {
        private class ConnexionCommande
        {
            private MySqlConnection m_laConnexion;
            private MySqlCommand m_laCommande;

            public ConnexionCommande(
                MySqlConnection laConnexion, MySqlCommand laCommande)
            {
                m_laConnexion = laConnexion;
                m_laCommande = laCommande;
            }


            public MySqlConnection Connexion
            {
                get { return m_laConnexion; }
            }

            public MySqlCommand Commande
            {
                get { return m_laCommande; }
            }
        }

        private const string INFOS_CONNEXION =
            "server=localhost;database=14b_travail3;uid=usager;pwd=abcdef";
        private static List<ConnexionCommande> g_lesConnexionsCommandes =
            new List<ConnexionCommande>();

        public static void PreparerRequetes()
        {
            MySqlCommand laCommande;
            MySqlConnection laConnexion;

            laConnexion = new MySqlConnection(INFOS_CONNEXION);
            laConnexion.Open();

            // Liste des requêtes SQL
            string[] strRequetes = new string[] {
                "insert into equipes values (@Code, @Nom, @Endroit);",
                "delete from equipes where Code = @Code;",
                "insert into joueurs (Nom, Prenom, Position) values (@Nom, " +
                "@Prenom, @Position);",
                "update joueurs set CodeEquipe=@CodeEquipe where Nom=@Nom " +
                "and Prenom=@Prenom and Position=@Position and CodeEquipe is null;"
                     };
            // Liste des paramètres pour chaque requête
            string[][] strParams = new string[][] {
                new string[] { "@Code", "@Nom", "@Endroit" },
                new string[] { "@Code" },
                new string[] { "@Nom", "@Prenom", "@Position" },
                new string[] { "@CodeEquipe", "@Nom", "@Prenom", "@Position" }
            };
            // Liste des types de chaque paramètre
            MySqlDbType[][] lesTypes = new MySqlDbType[][]
            {
                new MySqlDbType[] {
                    MySqlDbType.String,
                    MySqlDbType.VarChar,
                    MySqlDbType.VarChar },
                new MySqlDbType[] {
                    MySqlDbType.String },
                new MySqlDbType[] {
                    MySqlDbType.VarChar,
                    MySqlDbType.VarChar,
                    MySqlDbType.String },
                new MySqlDbType[] {
                    MySqlDbType.String,
                    MySqlDbType.VarChar,
                    MySqlDbType.VarChar,
                    MySqlDbType.String
                }
            };
            // Préparation de chaque requête
            for (int i = 0; i < strRequetes.Length; i++)
            {
                laCommande = new MySqlCommand(strRequetes[i], laConnexion);
                laCommande.Prepare();
                for (int j = 0; j < strParams[i].Length; j++)
                {
                    laCommande.Parameters.Add(strParams[i][j], lesTypes[i][j]);
                }
                g_lesConnexionsCommandes.Add(
                    new ConnexionCommande(laConnexion, laCommande));
            }
        }

        public static void LibererRequetes()
        {
            foreach (ConnexionCommande laRequete in g_lesConnexionsCommandes)
            {
                laRequete.Connexion.Close();
            }
            g_lesConnexionsCommandes.Clear();
        }

        public static List<Equipe> ObtenirEquipes()
        {
            MySqlCommand laCommande;
            MySqlConnection laConnexion;
            MySqlDataReader leLecteur;
            List<Equipe> lesEquipes = new List<Equipe>();

            laConnexion = new MySqlConnection(INFOS_CONNEXION);
            laConnexion.Open();

            laCommande = new MySqlCommand(
                "select e.*, j.Nom as NomJoueur, j.Prenom, j.Position from " +
                "equipes e left join joueurs j on Code = CodeEquipe;",
                laConnexion);
            leLecteur = laCommande.ExecuteReader();

            while (leLecteur.Read())
            {
                int iIndice = 0;
                Equipe uneEquipe = new Equipe(
                    leLecteur.GetString("Code"),
                    leLecteur.GetString("Nom"),
                    leLecteur.GetString("Endroit")
                    );
                if (lesEquipes.Contains(uneEquipe))
                {
                    bool bTrouve = false;
                    do
                    {
                        if (lesEquipes[iIndice].Code == uneEquipe.Code)
                        {
                            bTrouve = true;
                        }
                        else
                        {
                            iIndice++;
                        }
                    } while (!bTrouve);
                }
                else
                {
                    lesEquipes.Add(uneEquipe);
                    // Dernier élément de la liste
                    iIndice = lesEquipes.Count - 1;
                }

                // Si au moins un joueur est dans l'équipe
                if (!leLecteur.IsDBNull(leLecteur.GetOrdinal("NomJoueur")))
                {
                    Position laPosition;
                    switch (leLecteur.GetChar("Position"))
                    {
                        case 'C':
                            laPosition = Position.Centre;
                            break;
                        case 'L':
                            laPosition = Position.AilierGauche;
                            break;
                        case 'R':
                            laPosition = Position.AilierDroit;
                            break;
                        case 'D':
                            laPosition = Position.Defenseur;
                            break;
                        case 'G':
                            laPosition = Position.GardienDeBut;
                            break;
                        case 'I':
                            laPosition = Position.ADeterminer;
                            break;
                        default:
                            laPosition = Position.ADeterminer;
                            break;
                    }
                    lesEquipes[iIndice].Ajouter(new Joueur(
                        leLecteur.GetString("NomJoueur"),
                        leLecteur.GetString("Prenom"),
                        laPosition
                        ));
                }
            }

            return lesEquipes;
        }

        public static List<Joueur> ObtenirAgentsLibres()
        {
            MySqlCommand laCommande;
            MySqlConnection laConnexion;
            MySqlDataReader leLecteur;
            List<Joueur> lesJoueursLibres = new List<Joueur>();

            laConnexion = new MySqlConnection(INFOS_CONNEXION);
            laConnexion.Open();

            laCommande = new MySqlCommand(
                "select Nom, Prenom, Position from joueurs where CodeEquipe " +
                "is null;",
                laConnexion);
            leLecteur = laCommande.ExecuteReader();

            while (leLecteur.Read())
            {
                Position laPosition;
                switch (leLecteur.GetChar("Position"))
                {
                    case 'C':
                        laPosition = Position.Centre;
                        break;
                    case 'L':
                        laPosition = Position.AilierGauche;
                        break;
                    case 'R':
                        laPosition = Position.AilierDroit;
                        break;
                    case 'D':
                        laPosition = Position.Defenseur;
                        break;
                    case 'G':
                        laPosition = Position.GardienDeBut;
                        break;
                    case 'I':
                        laPosition = Position.ADeterminer;
                        break;
                    default:
                        laPosition = Position.ADeterminer;
                        break;
                }

                lesJoueursLibres.Add(new Joueur(
                    leLecteur.GetString("Nom"),
                    leLecteur.GetString("Prenom"),
                    laPosition
                    ));
            }

            return lesJoueursLibres;
        }

        public static void AjouterEquipe(Equipe uneEquipe)
        {
            // Pour ne pas avoir à modifier toutes les instances si on modifie
            // les requêtes
            int iRequete = 0;

            g_lesConnexionsCommandes[iRequete].Commande
                .Parameters["@Code"].Value = uneEquipe.Code;
            g_lesConnexionsCommandes[iRequete].Commande
                .Parameters["@Nom"].Value = uneEquipe.Nom;
            g_lesConnexionsCommandes[iRequete].Commande
                .Parameters["@Endroit"].Value = uneEquipe.Endroit;

            g_lesConnexionsCommandes[iRequete].Commande.ExecuteNonQuery();
        }

        public static void SupprimerEquipe(Equipe uneEquipe)
        {
            // Pour ne pas avoir à modifier toutes les instances si on modifie
            // les requêtes
            int iRequete = 1;

            g_lesConnexionsCommandes[iRequete].Commande
                .Parameters["@Code"].Value = uneEquipe.Code;

            g_lesConnexionsCommandes[iRequete].Commande.ExecuteNonQuery();
        }

        public static void AjouterJoueur(Joueur unJoueur)
        {
            // Pour ne pas avoir à modifier toutes les instances si on modifie
            // les requêtes
            int iRequete = 2;
            g_lesConnexionsCommandes[iRequete].Commande
                .Parameters["@Nom"].Value = unJoueur.Nom;
            g_lesConnexionsCommandes[iRequete].Commande
                .Parameters["@Prenom"].Value = unJoueur.Prenom;
            char cPos;
            switch (unJoueur.Position)
            {
                case Position.Centre:
                    cPos = 'C';
                    break;
                case Position.AilierGauche:
                    cPos = 'L';
                    break;
                case Position.AilierDroit:
                    cPos = 'R';
                    break;
                case Position.Defenseur:
                    cPos = 'D';
                    break;
                case Position.GardienDeBut:
                    cPos = 'G';
                    break;
                case Position.ADeterminer:
                    cPos = 'I';
                    break;
                default:
                    cPos = ' ';
                    break;
            }
            g_lesConnexionsCommandes[iRequete].Commande
                .Parameters["@Position"].Value = cPos;

            g_lesConnexionsCommandes[iRequete].Commande.ExecuteNonQuery();
        }

        public static void AffecterJoueurAEquipe(
            Equipe uneEquipe, Joueur unJoueur)
        {
            // Pour ne pas avoir à modifier toutes les instances si on modifie
            // les requêtes
            int iRequete = 3;
            g_lesConnexionsCommandes[iRequete].Commande
                .Parameters["@Nom"].Value = unJoueur.Nom;
            g_lesConnexionsCommandes[iRequete].Commande
                .Parameters["@Prenom"].Value = unJoueur.Prenom;
            char cPos;
            switch (unJoueur.Position)
            {
                case Position.Centre:
                    cPos = 'C';
                    break;
                case Position.AilierGauche:
                    cPos = 'L';
                    break;
                case Position.AilierDroit:
                    cPos = 'R';
                    break;
                case Position.Defenseur:
                    cPos = 'D';
                    break;
                case Position.GardienDeBut:
                    cPos = 'G';
                    break;
                case Position.ADeterminer:
                    cPos = 'I';
                    break;
                default:
                    cPos = ' ';
                    break;
            }
            g_lesConnexionsCommandes[iRequete].Commande
                .Parameters["@Position"].Value = cPos;
            g_lesConnexionsCommandes[iRequete].Commande
                .Parameters["@CodeEquipe"].Value = uneEquipe.Code;

            g_lesConnexionsCommandes[iRequete].Commande.ExecuteNonQuery();
        }
    }
}
