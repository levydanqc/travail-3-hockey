﻿/*******************************************************************************
 * Fichier : programme.cs
 * 
 * Classe : Programme
 * 
 * Description : Point de démarrage du programme de gestion de données d'une
 * équipe de hockey.
 * 
 * Auteur : Dan Lévy
 * 
 * GitLab : https://gitlab.com/levydanqc/travail-3-hockey
*******************************************************************************/

using System;
using System.Windows.Forms;

namespace travail3
{
    public static class Programme
    {
        /// <summary>
        /// Point d'entrée principal de l'application.
        /// </summary>
        [STAThread]
        private static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new FenetrePrincipale());
        }
    }
}
