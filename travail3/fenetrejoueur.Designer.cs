﻿namespace travail3
{
    partial class FenetreJoueur
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose (bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose ();
            }
            base.Dispose (disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent ()
        {
            this.m_grpPosition = new System.Windows.Forms.GroupBox();
            this.m_optADeterminer = new System.Windows.Forms.RadioButton();
            this.m_optDefenseur = new System.Windows.Forms.RadioButton();
            this.m_optAilierDroit = new System.Windows.Forms.RadioButton();
            this.m_optAilierGauche = new System.Windows.Forms.RadioButton();
            this.m_optCentre = new System.Windows.Forms.RadioButton();
            this.m_txtPrenom = new System.Windows.Forms.TextBox();
            this.m_lblPrenom = new System.Windows.Forms.Label();
            this.m_txtNom = new System.Windows.Forms.TextBox();
            this.m_lblNom = new System.Windows.Forms.Label();
            this.m_btnAnnuler = new System.Windows.Forms.Button();
            this.m_btnOK = new System.Windows.Forms.Button();
            this.m_optGardienDeBut = new System.Windows.Forms.RadioButton();
            this.m_grpPosition.SuspendLayout();
            this.SuspendLayout();
            // 
            // m_grpPosition
            // 
            this.m_grpPosition.Controls.Add(this.m_optGardienDeBut);
            this.m_grpPosition.Controls.Add(this.m_optADeterminer);
            this.m_grpPosition.Controls.Add(this.m_optDefenseur);
            this.m_grpPosition.Controls.Add(this.m_optAilierDroit);
            this.m_grpPosition.Controls.Add(this.m_optAilierGauche);
            this.m_grpPosition.Controls.Add(this.m_optCentre);
            this.m_grpPosition.Location = new System.Drawing.Point(230, 9);
            this.m_grpPosition.Name = "m_grpPosition";
            this.m_grpPosition.Size = new System.Drawing.Size(200, 169);
            this.m_grpPosition.TabIndex = 4;
            this.m_grpPosition.TabStop = false;
            this.m_grpPosition.Text = "P&osition:";
            // 
            // m_optADeterminer
            // 
            this.m_optADeterminer.AutoSize = true;
            this.m_optADeterminer.Location = new System.Drawing.Point(6, 141);
            this.m_optADeterminer.Name = "m_optADeterminer";
            this.m_optADeterminer.Size = new System.Drawing.Size(86, 17);
            this.m_optADeterminer.TabIndex = 5;
            this.m_optADeterminer.Text = "À Déterminer";
            this.m_optADeterminer.UseVisualStyleBackColor = true;
            // 
            // m_optDefenseur
            // 
            this.m_optDefenseur.AutoSize = true;
            this.m_optDefenseur.Location = new System.Drawing.Point(7, 95);
            this.m_optDefenseur.Name = "m_optDefenseur";
            this.m_optDefenseur.Size = new System.Drawing.Size(74, 17);
            this.m_optDefenseur.TabIndex = 3;
            this.m_optDefenseur.Text = "Défenseur";
            this.m_optDefenseur.UseVisualStyleBackColor = true;
            // 
            // m_optAilierDroit
            // 
            this.m_optAilierDroit.AutoSize = true;
            this.m_optAilierDroit.Location = new System.Drawing.Point(7, 70);
            this.m_optAilierDroit.Name = "m_optAilierDroit";
            this.m_optAilierDroit.Size = new System.Drawing.Size(72, 17);
            this.m_optAilierDroit.TabIndex = 2;
            this.m_optAilierDroit.Text = "Ailier Droit";
            this.m_optAilierDroit.UseVisualStyleBackColor = true;
            // 
            // m_optAilierGauche
            // 
            this.m_optAilierGauche.AutoSize = true;
            this.m_optAilierGauche.Location = new System.Drawing.Point(7, 45);
            this.m_optAilierGauche.Name = "m_optAilierGauche";
            this.m_optAilierGauche.Size = new System.Drawing.Size(88, 17);
            this.m_optAilierGauche.TabIndex = 1;
            this.m_optAilierGauche.Text = "Ailier Gauche";
            this.m_optAilierGauche.UseVisualStyleBackColor = true;
            // 
            // m_optCentre
            // 
            this.m_optCentre.AutoSize = true;
            this.m_optCentre.Location = new System.Drawing.Point(7, 20);
            this.m_optCentre.Name = "m_optCentre";
            this.m_optCentre.Size = new System.Drawing.Size(56, 17);
            this.m_optCentre.TabIndex = 0;
            this.m_optCentre.Text = "Centre";
            this.m_optCentre.UseVisualStyleBackColor = true;
            // 
            // m_txtPrenom
            // 
            this.m_txtPrenom.Location = new System.Drawing.Point(12, 80);
            this.m_txtPrenom.Name = "m_txtPrenom";
            this.m_txtPrenom.Size = new System.Drawing.Size(212, 20);
            this.m_txtPrenom.TabIndex = 3;
            // 
            // m_lblPrenom
            // 
            this.m_lblPrenom.AutoSize = true;
            this.m_lblPrenom.Location = new System.Drawing.Point(12, 64);
            this.m_lblPrenom.Name = "m_lblPrenom";
            this.m_lblPrenom.Size = new System.Drawing.Size(46, 13);
            this.m_lblPrenom.TabIndex = 2;
            this.m_lblPrenom.Text = "&Prénom:";
            // 
            // m_txtNom
            // 
            this.m_txtNom.Location = new System.Drawing.Point(12, 25);
            this.m_txtNom.Name = "m_txtNom";
            this.m_txtNom.Size = new System.Drawing.Size(212, 20);
            this.m_txtNom.TabIndex = 1;
            // 
            // m_lblNom
            // 
            this.m_lblNom.AutoSize = true;
            this.m_lblNom.Location = new System.Drawing.Point(12, 9);
            this.m_lblNom.Name = "m_lblNom";
            this.m_lblNom.Size = new System.Drawing.Size(32, 13);
            this.m_lblNom.TabIndex = 0;
            this.m_lblNom.Text = "&Nom:";
            // 
            // m_btnAnnuler
            // 
            this.m_btnAnnuler.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.m_btnAnnuler.Location = new System.Drawing.Point(233, 198);
            this.m_btnAnnuler.Name = "m_btnAnnuler";
            this.m_btnAnnuler.Size = new System.Drawing.Size(75, 23);
            this.m_btnAnnuler.TabIndex = 6;
            this.m_btnAnnuler.Text = "&Annuler";
            this.m_btnAnnuler.UseVisualStyleBackColor = true;
            // 
            // m_btnOK
            // 
            this.m_btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.m_btnOK.Location = new System.Drawing.Point(139, 198);
            this.m_btnOK.Name = "m_btnOK";
            this.m_btnOK.Size = new System.Drawing.Size(75, 23);
            this.m_btnOK.TabIndex = 5;
            this.m_btnOK.Text = "&OK";
            this.m_btnOK.UseVisualStyleBackColor = true;
            this.m_btnOK.Click += new System.EventHandler(this.m_btnOK_Click);
            // 
            // m_optGardienDeBut
            // 
            this.m_optGardienDeBut.AutoSize = true;
            this.m_optGardienDeBut.Location = new System.Drawing.Point(6, 118);
            this.m_optGardienDeBut.Name = "m_optGardienDeBut";
            this.m_optGardienDeBut.Size = new System.Drawing.Size(95, 17);
            this.m_optGardienDeBut.TabIndex = 4;
            this.m_optGardienDeBut.Text = "Gardien de but";
            this.m_optGardienDeBut.UseVisualStyleBackColor = true;
            // 
            // FenetreJoueur
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(446, 233);
            this.Controls.Add(this.m_grpPosition);
            this.Controls.Add(this.m_txtPrenom);
            this.Controls.Add(this.m_lblPrenom);
            this.Controls.Add(this.m_txtNom);
            this.Controls.Add(this.m_lblNom);
            this.Controls.Add(this.m_btnAnnuler);
            this.Controls.Add(this.m_btnOK);
            this.Name = "FenetreJoueur";
            this.Text = "fenetrejoueur";
            this.m_grpPosition.ResumeLayout(false);
            this.m_grpPosition.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label m_lblNom;
        private System.Windows.Forms.TextBox m_txtNom;
        private System.Windows.Forms.Label m_lblPrenom;
        private System.Windows.Forms.TextBox m_txtPrenom;
        private System.Windows.Forms.GroupBox m_grpPosition;
        private System.Windows.Forms.RadioButton m_optCentre;
        private System.Windows.Forms.RadioButton m_optAilierGauche;
        private System.Windows.Forms.RadioButton m_optAilierDroit;
        private System.Windows.Forms.RadioButton m_optDefenseur;
        private System.Windows.Forms.RadioButton m_optGardienDeBut;
        private System.Windows.Forms.RadioButton m_optADeterminer;
        private System.Windows.Forms.Button m_btnOK;
        private System.Windows.Forms.Button m_btnAnnuler;        
    }
}