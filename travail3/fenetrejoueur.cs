﻿/*******************************************************************************
 * Fichier : fenetrejoueur.cs
 * 
 * Classe : FenetreJoueur
 * 
 * Description : Fenêtre d'ajout d'un joueur de hockey.
 * 
 * Auteur : Dan Lévy
 * 
 * GitLab : https://gitlab.com/levydanqc/travail-3-hockey
*******************************************************************************/

using System;
using System.Windows.Forms;
using Hockey;

namespace travail3
{
    public partial class FenetreJoueur : Form
    {
        private Joueur m_leJoueur;

        public FenetreJoueur ()
        {
            InitializeComponent ();
            FormBorderStyle = FormBorderStyle.FixedDialog;
            MinimizeBox = false;
            MaximizeBox = false;
            ShowInTaskbar = false;
            AcceptButton = m_btnOK;
            CancelButton = m_btnAnnuler;
            m_btnOK.DialogResult = DialogResult.OK;
            m_btnAnnuler.DialogResult = DialogResult.Cancel;
            m_optADeterminer.Checked = true;
        }

        public Joueur LeJoueur
        {
            get { return m_leJoueur; }
        }

        private void m_btnOK_Click (object sender, EventArgs e)
        {
            Position laPosition;

            if (m_optCentre.Checked)
            {
                laPosition = Position.Centre;
            }
            else if (m_optAilierGauche.Checked)
            {
                laPosition = Position.AilierGauche;
            }
            else if (m_optAilierDroit.Checked)
            {
                laPosition = Position.AilierDroit;
            }
            else if (m_optDefenseur.Checked)
            {
                laPosition = Position.Defenseur;
            }
            else if (m_optGardienDeBut.Checked)
            {
                laPosition = Position.GardienDeBut;
            }
            else
            {
                laPosition = Position.ADeterminer;
            }
            try
            {
                m_leJoueur = new Joueur (m_txtNom.Text, m_txtPrenom.Text,
                    laPosition);
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show (ex.Message,
                    FenetrePrincipale.TITRE_APPLICATION, MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
                DialogResult = DialogResult.None;
            }
        }
    }
}
