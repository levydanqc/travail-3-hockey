﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Hockey;

namespace travail3_tests
{
    [TestClass]
    public class TestsEnumerationPosition
    {
        [TestMethod]
        public void TesterPosition ()
        {
            string [] strLesNoms = Enum.GetNames (typeof (Position));
            int [] iLesValeurs = (int []) Enum.GetValues (typeof (Position));
            int i;

            Assert.AreEqual (6, strLesNoms.Length);
            Assert.AreEqual (6, iLesValeurs.Length);
            Assert.AreEqual ("Centre", strLesNoms [0]);
            Assert.AreEqual ("AilierGauche", strLesNoms [1]);
            Assert.AreEqual ("AilierDroit", strLesNoms [2]);
            Assert.AreEqual ("Defenseur", strLesNoms [3]);
            Assert.AreEqual ("GardienDeBut", strLesNoms [4]);
            Assert.AreEqual ("ADeterminer", strLesNoms [5]);
            for (i = 0; i <= 5; i++)
            {
                Assert.AreEqual (i, iLesValeurs [i]);
            }
        }
    }
}
