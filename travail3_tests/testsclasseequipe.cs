﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Hockey;

namespace travail3_tests
{
    [TestClass]
    public class TestsClasseEquipe
    {
        [TestMethod]
        public void TesterConstructeur ()
        {
            Equipe uneEquipe;
            string [] strLesCodes;
            string [] strLesNoms;
            string [] strLesEndroits;
            char [] cLesLettres = new char [] {'Q', 'U', 'E'};
            int i;
            int j;
            char cTempo;
            string strCodeTest;


            strLesCodes = new string [] {"que", "quE", "qUe", "qUE", "Que",
                "QuE", "QUe", "QUE"};
            strLesNoms = new string [] {"Nordiques", "N",
                "abcdefghijklmnopqrstuvwxyzabcdef"};
            strLesEndroits = new string [] {"Quebec", "Q",
                "abcdefghijklmnopqrstuvwxyzabcdef"};

            /******************************************************************
             Cas 1: Paramètres valides.
            ******************************************************************/
            foreach (string strCode in strLesCodes)
            {
                foreach (string strNom in strLesNoms)
                {
                    foreach (string strEndroit in strLesEndroits)
                    {
                        uneEquipe = new Equipe (strCode, strNom, strEndroit);
                        Assert.AreEqual (strCode.ToUpper (), uneEquipe.Code);
                        Assert.AreEqual (strNom, uneEquipe.Nom);
                        Assert.AreEqual (strEndroit, uneEquipe.Endroit);
                    }
                }
            }

            uneEquipe = null;
            strLesCodes = new string [] {null, "", "QUEB", "1QUE", "Q1UE",
                "QU1E", "QUE1"};
            strLesNoms = new string [] {"Nordiques", null, "",
                "abcdefghijklmnopqrstuvwxyzabcdefg"};
            strLesEndroits = new string [] {"Quebec", null, "",
                "abcdefghijklmnopqrstuvwxyzabcdefg"};

            /******************************************************************
             Cas 2: Premier paramètre invalide.
            ******************************************************************/
            foreach (string strCode in strLesCodes)
            {
                foreach (string strNom in strLesNoms)
                {
                    foreach (string strEndroit in strLesEndroits)
                    {
                        try
                        {
                            uneEquipe = new Equipe (strCode, strNom,
                                strEndroit);
                            Assert.Fail ();
                        }
                        catch (ArgumentException ex)
                        {
                            Assert.IsNull (uneEquipe);
                            if (ex is ArgumentNullException)
                            {
                                Assert.AreEqual ("Le code est nul.",
                                    ex.Message);
                            }
                            else
                            {
                                Assert.AreEqual ("La longueur du code est "
                                    + "invalide.", ex.Message);
                            }
                        }
                    }
                }
            }
            for (i = 0; i < cLesLettres.Length; i++)
            {
                for (j = 0; j < 'A'; j++)
                {
                    cTempo = cLesLettres [i];
                    cLesLettres [i] = (char) j;
                    strCodeTest = new string (cLesLettres);
                    cLesLettres [i] = cTempo;
                    foreach (string strNom in strLesNoms)
                    {
                        foreach (string strEndroit in strLesEndroits)
                        {
                            try
                            {
                                uneEquipe = new Equipe (strCodeTest, strNom,
                                    strEndroit);
                                Assert.Fail ();
                            }
                            catch (ArgumentException ex)
                            {
                                Assert.IsNull (uneEquipe);
                                Assert.AreEqual ("Le code contient au moins "
                                    + "un caractère invalide.", ex.Message);
                            }
                        }
                    }
                }
                for (j = 'Z' + 1; j < 'a'; j++)
                {
                    cTempo = cLesLettres [i];
                    cLesLettres [i] = (char) j;
                    strCodeTest = new string (cLesLettres);
                    cLesLettres [i] = cTempo;
                    foreach (string strNom in strLesNoms)
                    {
                        foreach (string strEndroit in strLesEndroits)
                        {
                            try
                            {
                                uneEquipe = new Equipe (strCodeTest, strNom,
                                    strEndroit);
                                Assert.Fail ();
                            }
                            catch (ArgumentException ex)
                            {
                                Assert.IsNull (uneEquipe);
                                Assert.AreEqual ("Le code contient au moins "
                                    + "un caractère invalide.", ex.Message);
                            }
                        }
                    }
                }

                for (j = 'z' + 1; j < char.MaxValue; j++)
                {
                    cTempo = cLesLettres [i];
                    cLesLettres [i] = (char) j;
                    strCodeTest = new string (cLesLettres);
                    cLesLettres [i] = cTempo;
                    foreach (string strNom in strLesNoms)
                    {
                        foreach (string strEndroit in strLesEndroits)
                        {
                            try
                            {
                                uneEquipe = new Equipe (strCodeTest, strNom,
                                    strEndroit);
                                Assert.Fail ();
                            }
                            catch (ArgumentException ex)
                            {
                                Assert.IsNull (uneEquipe);
                                Assert.AreEqual ("Le code contient au moins "
                                    + "un caractère invalide.", ex.Message);
                            }
                        }
                    }
                }
            }

            strLesNoms = new string [] {null, "",
                "abcdefghijklmnopqrstuvwxyzabcdefg"};

            /******************************************************************
             Cas 3: Deuxième paramètre invalide.
            ******************************************************************/
            foreach (string strNom in strLesNoms)
            {
                foreach (string strEndroit in strLesEndroits)
                {
                    try
                    {
                        uneEquipe = new Equipe ("QUE", strNom, strEndroit);
                        Assert.Fail ();
                    }
                    catch (ArgumentException ex)
                    {
                        Assert.IsNull (uneEquipe);
                        if (ex is ArgumentNullException)
                        {
                            Assert.AreEqual ("Le nom est nul.", ex.Message);
                        }
                        else
                        {
                            Assert.AreEqual ("La longueur du nom est "
                                + "invalide.", ex.Message);
                        }
                    }
                }
            }

            strLesEndroits = new string [] {null, "",
                "abcdefghijklmnopqrstuvwxyzabcdefg"};

            /******************************************************************
             Cas 4: Troisième paramètre invalide.
            ******************************************************************/
            foreach (string strEndroit in strLesEndroits)
            {
                try
                {
                    uneEquipe = new Equipe ("QUE", "Nordiques", strEndroit);
                    Assert.Fail ();
                }
                catch (ArgumentException ex)
                {
                    Assert.IsNull (uneEquipe);
                    if (ex is ArgumentNullException)
                    {
                        Assert.AreEqual ("L'endroit est nul.", ex.Message);
                    }
                    else
                    {
                        Assert.AreEqual ("La longueur de l'endroit est "
                            + "invalide.", ex.Message);
                    }
                }
            }
        }

        [TestMethod]
        public void TesterMethodeToString ()
        {
            Equipe uneEquipe = new Equipe ("QUE", "Nordiques", "Quebec");

            Assert.AreEqual ("Nordiques de Quebec", uneEquipe.ToString ());
        }

        [TestMethod]
        public void TesterOperateurComparaisonEgalite ()
        {
            Equipe uneEquipe;
            Equipe uneAutreEquipe;

            uneEquipe = new Equipe ("QUE", "Nordiques", "Quebec");
            uneAutreEquipe = uneEquipe;
            Assert.IsTrue (uneEquipe == uneAutreEquipe);
            Assert.IsTrue (uneAutreEquipe == uneEquipe);

            uneAutreEquipe = null;
            Assert.IsFalse (uneAutreEquipe == uneEquipe);
            Assert.IsFalse (uneEquipe == uneAutreEquipe);

            uneEquipe = null;
            Assert.IsTrue (uneEquipe == uneAutreEquipe);
            Assert.IsTrue (uneAutreEquipe == uneEquipe);

            uneEquipe = new Equipe ("QUE", "Nordiques", "Quebec");
            uneAutreEquipe = new Equipe ("QUE", "Canadiens", "Montreal");
            Assert.IsTrue (uneEquipe == uneAutreEquipe);
            Assert.IsTrue (uneAutreEquipe == uneEquipe);

            uneAutreEquipe = new Equipe ("MTL", "Canadiens", "Montreal");
            Assert.IsFalse (uneEquipe == uneAutreEquipe);
            Assert.IsFalse (uneAutreEquipe == uneEquipe);
        }

        [TestMethod]
        public void TesterOperateurComparaisonDifference ()
        {
            Equipe uneEquipe;
            Equipe uneAutreEquipe;

            uneEquipe = new Equipe ("QUE", "Nordiques", "Quebec");
            uneAutreEquipe = uneEquipe;
            Assert.IsFalse (uneEquipe != uneAutreEquipe);
            Assert.IsFalse (uneAutreEquipe != uneEquipe);

            uneAutreEquipe = null;
            Assert.IsTrue (uneAutreEquipe != uneEquipe);
            Assert.IsTrue (uneEquipe != uneAutreEquipe);

            uneEquipe = null;
            Assert.IsFalse (uneEquipe != uneAutreEquipe);
            Assert.IsFalse (uneAutreEquipe != uneEquipe);

            uneEquipe = new Equipe ("QUE", "Nordiques", "Quebec");
            uneAutreEquipe = new Equipe ("QUE", "Canadiens", "Montreal");
            Assert.IsFalse (uneEquipe != uneAutreEquipe);
            Assert.IsFalse (uneAutreEquipe != uneEquipe);

            uneAutreEquipe = new Equipe ("MTL", "Canadiens", "Montreal");
            Assert.IsTrue (uneEquipe != uneAutreEquipe);
            Assert.IsTrue (uneAutreEquipe != uneEquipe);
        }

        [TestMethod]
        public void TesterMethodeEquals ()
        {
            Equipe uneEquipe;
            Equipe uneAutreEquipe;

            uneEquipe = new Equipe ("QUE", "Nordiques", "Quebec");
            uneAutreEquipe = uneEquipe;
            Assert.IsTrue (uneEquipe.Equals (uneAutreEquipe));
            Assert.IsTrue (uneAutreEquipe.Equals (uneEquipe));

            uneAutreEquipe = null;
            Assert.IsFalse (uneEquipe.Equals (uneAutreEquipe));

            uneEquipe = new Equipe ("QUE", "Nordiques", "Quebec");
            uneAutreEquipe = new Equipe ("QUE", "Canadiens", "Montreal");
            Assert.IsTrue (uneEquipe.Equals (uneAutreEquipe));
            Assert.IsTrue (uneAutreEquipe.Equals (uneEquipe));

            uneAutreEquipe = new Equipe ("MTL", "Canadiens", "Montreal");
            Assert.IsFalse (uneEquipe.Equals (uneAutreEquipe));
            Assert.IsFalse (uneAutreEquipe.Equals (uneEquipe));

            Assert.IsFalse (uneEquipe.Equals ("QUE"));
            Assert.IsFalse (uneEquipe.Equals (1));
        }

        [TestMethod]
        public void TesterMethodeObtenirJoueur ()
        {
            Equipe uneEquipe = new Equipe ("QUE", "Nordiques", "Quebec");
            Joueur unJoueur = new Joueur ("Tremblay", "Pierre",
                Position.Centre);
            Joueur unAutreJoueur = new Joueur ("Allard", "François",
                Position.Defenseur);
            int i;

            uneEquipe.Ajouter (unJoueur);
            uneEquipe.Ajouter (unAutreJoueur);

            Assert.AreEqual (unJoueur, uneEquipe.ObtenirJoueur (0));
            Assert.AreEqual (unAutreJoueur, uneEquipe.ObtenirJoueur (1));
            i = 0;
            while (uneEquipe.ObtenirJoueur (i) != null)
            {
                i++;
            }
            Assert.AreEqual (2, i);

            Assert.IsNull (uneEquipe.ObtenirJoueur (-1));

            Assert.IsNull (uneEquipe.ObtenirJoueur (2));
        }

        [TestMethod]
        public void TesterMethodeAjouter ()
        {
            Equipe uneEquipe = new Equipe ("QUE", "Nordiques", "Quebec");
            Joueur unJoueur = new Joueur ("Tremblay", "Pierre",
                Position.Centre);
            Joueur unAutreJoueur;
            int i;

            Assert.IsTrue (uneEquipe.Ajouter (unJoueur));
            unAutreJoueur = uneEquipe.ObtenirJoueur (0);
            Assert.AreEqual ("Tremblay", unAutreJoueur.Nom);
            Assert.AreEqual ("Pierre", unAutreJoueur.Prenom);
            Assert.AreEqual (Position.Centre, unAutreJoueur.Position);

            Assert.IsFalse (uneEquipe.Ajouter (null));
            i = 0;
            while (uneEquipe.ObtenirJoueur (i) != null)
            {
                i++;
            }
            Assert.AreEqual (1, i);

            Assert.IsFalse (uneEquipe.Ajouter (unJoueur));
            i = 0;
            while (uneEquipe.ObtenirJoueur (i) != null)
            {
                i++;
            }
            Assert.AreEqual (1, i);
        }

        [TestMethod]
        public void TesterMethodeRetirer ()
        {
            Equipe uneEquipe = new Equipe ("QUE", "Nordiques", "Quebec");
            Joueur unJoueur;
            int i;

            uneEquipe.Ajouter (new Joueur ("Tremblay", "Pierre",
                Position.Centre));
            uneEquipe.Ajouter (new Joueur ("Allard", "François",
                Position.Defenseur));
            unJoueur = uneEquipe.ObtenirJoueur (0);

            Assert.IsTrue (uneEquipe.Retirer (unJoueur));
            i = 0;
            while (uneEquipe.ObtenirJoueur (i) != null)
            {
                i++;
            }
            Assert.AreEqual (1, i);

            Assert.IsFalse (uneEquipe.Retirer (unJoueur));
            i = 0;
            while (uneEquipe.ObtenirJoueur (i) != null)
            {
                i++;
            }
            Assert.AreEqual (1, i);
        }
    }
}
