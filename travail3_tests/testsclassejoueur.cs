﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Hockey;

namespace travail3_tests
{
    [TestClass]
    public class TestsClasseJoueur
    {
        [TestMethod]
        public void TesterConstructeur ()
        {
            Joueur unJoueur;
            string [] strLesNoms;
            string [] strLesPrenoms;
            Position [] lesPositions;

            strLesNoms = new string [] {"Tremblay", "T",
                "abcdefghijklmnopqrstuvwxyzabcdef"};
            strLesPrenoms = new string [] {"Pierre", "P",
                "abcdefghijklmnopqrstuvwxyzabcdef"};
            
            /******************************************************************
             Cas 1: Paramètres valides.
            ******************************************************************/
            foreach (string strNom in strLesNoms)
            {
                foreach (string strPrenom in strLesPrenoms)
                {
                    foreach (Position unePosition in Enum.GetValues
                        (typeof (Position)))
                    {
                        unJoueur = new Joueur (strNom, strPrenom, unePosition);
                        Assert.AreEqual (strNom, unJoueur.Nom);
                        Assert.AreEqual (strPrenom, unJoueur.Prenom);
                        Assert.AreEqual (unePosition, unJoueur.Position);
                    }
                }
            }

            unJoueur = null;
            strLesNoms = new string [] {null, "",
                "abcdefghijklmnopqrstuvwxyzabcdefg"};
            strLesPrenoms = new string [] {"Pierre", null , "",
                "abcdefghijklmnopqrstuvwxyzabcdefg"};
            lesPositions = new Position [] {Position.Centre, (Position) 6};

            /******************************************************************
             Cas 2: Premier paramètre invalide.
            ******************************************************************/
            foreach (string strNom in strLesNoms)
            {
                foreach (string strPrenom in strLesPrenoms)
                {
                    foreach (Position unePosition in lesPositions)
                    {
                        try
                        {
                            unJoueur = new Joueur (strNom, strPrenom,
                                unePosition);
                            Assert.Fail ();
                        }
                        catch (ArgumentException ex)
                        {
                            Assert.IsNull (unJoueur);
                            if (ex is ArgumentNullException)
                            {
                                Assert.AreEqual ("Le nom est nul.", ex.Message);
                            }
                            else
                            {
                                Assert.AreEqual ("La longueur du nom est "
                                + "invalide.", ex.Message);
                            }
                        }
                    }
                }
            }

            strLesPrenoms = new string [] {null , "",
                "abcdefghijklmnopqrstuvwxyzabcdefg"};

            /******************************************************************
             Cas 3: Deuxième paramètre invalide.
            ******************************************************************/
            foreach (string strPrenom in strLesPrenoms)
            {
                foreach (Position unePostion in lesPositions)
                {
                    try
                    {
                        unJoueur = new Joueur ("Tremblay", strPrenom,
                            unePostion);
                        Assert.Fail ();
                    }
                    catch (ArgumentException ex)
                    {
                        Assert.IsNull (unJoueur);
                        if (ex is ArgumentNullException)
                        {
                            Assert.AreEqual ("Le prénom est nul.", ex.Message);
                        }
                        else
                        {
                            Assert.AreEqual ("La longueur du prénom est "
                               + "invalide.", ex.Message);
                        }
                    }
                }
            }

            /******************************************************************
             Cas 4: Troisième paramètre invalide.
            ******************************************************************/
            try
            {
                unJoueur = new Joueur ("Tremblay", "Pierre", (Position) 6);
                Assert.Fail ();
            }
            catch (ArgumentException ex)
            {
                Assert.IsNull (unJoueur);
                Assert.AreEqual ("La position est invalide.", ex.Message);
            }
        }

        [TestMethod]
        public void TesterAccesseurNomEnEcriture ()
        {
            Joueur unJoueur = new Joueur ("Tremblay", "Pierre",
                Position.Centre);

            // Cas 1: Valeur valide.
            unJoueur.Nom = "Allard";
            Assert.AreEqual ("Allard", unJoueur.Nom);
            Assert.AreEqual ("Pierre", unJoueur.Prenom);
            Assert.AreEqual (Position.Centre, unJoueur.Position);

            // Cas 2: Valeur nulle.
            try
            {
                unJoueur.Nom = null;
                Assert.Fail ();
            }
            catch (ArgumentNullException ex)
            {
                Assert.AreEqual ("Allard", unJoueur.Nom);
                Assert.AreEqual ("Pierre", unJoueur.Prenom);
                Assert.AreEqual (Position.Centre, unJoueur.Position);
                Assert.AreEqual ("Le nom est nul.", ex.Message);
            }

            // Cas 3: Valeur vide.
            try
            {
                unJoueur.Nom = "";
                Assert.Fail ();
            }
            catch (ArgumentException ex)
            {
                Assert.AreEqual ("Allard", unJoueur.Nom);
                Assert.AreEqual ("Pierre", unJoueur.Prenom);
                Assert.AreEqual (Position.Centre, unJoueur.Position);
                Assert.AreEqual ("La longueur du nom est invalide.",
                    ex.Message);
            }

            // Cas 4: Valeur trop longue.
            try
            {
                unJoueur.Nom = "abcdefghijklmnopqrstuvwxyzabcdefg";
                Assert.Fail ();
            }
            catch (ArgumentException ex)
            {
                Assert.AreEqual ("Allard", unJoueur.Nom);
                Assert.AreEqual ("Pierre", unJoueur.Prenom);
                Assert.AreEqual (Position.Centre, unJoueur.Position);
                Assert.AreEqual ("La longueur du nom est invalide.",
                    ex.Message);
            }
        }

        [TestMethod]
        public void TesterAccesseurPrenomEnEcriture ()
        {
            Joueur unJoueur = new Joueur ("Tremblay", "Pierre",
                Position.Centre);

            // Cas 1: Valeur valide.
            unJoueur.Prenom = "François";
            Assert.AreEqual ("Tremblay", unJoueur.Nom);
            Assert.AreEqual ("François", unJoueur.Prenom);
            Assert.AreEqual (Position.Centre, unJoueur.Position);

            // Cas 2: Valeur nulle.
            try
            {
                unJoueur.Prenom = null;
                Assert.Fail ();
            }
            catch (ArgumentNullException ex)
            {
                Assert.AreEqual ("Tremblay", unJoueur.Nom);
                Assert.AreEqual ("François", unJoueur.Prenom);
                Assert.AreEqual (Position.Centre, unJoueur.Position);
                Assert.AreEqual ("Le prénom est nul.", ex.Message);
            }

            // Cas 3: Valeur vide.
            try
            {
                unJoueur.Prenom = "";
                Assert.Fail ();
            }
            catch (ArgumentException ex)
            {
                Assert.AreEqual ("Tremblay", unJoueur.Nom);
                Assert.AreEqual ("François", unJoueur.Prenom);
                Assert.AreEqual (Position.Centre, unJoueur.Position);
                Assert.AreEqual ("La longueur du prénom est invalide.",
                    ex.Message);
            }

            // Cas 4: Valeur trop longue.
            try
            {
                unJoueur.Prenom = "abcdefghijklmnopqrstuvwxyzabcdefg";
                Assert.Fail ();
            }
            catch (ArgumentException ex)
            {
                Assert.AreEqual ("Tremblay", unJoueur.Nom);
                Assert.AreEqual ("François", unJoueur.Prenom);
                Assert.AreEqual (Position.Centre, unJoueur.Position);
                Assert.AreEqual ("La longueur du prénom est invalide.",
                    ex.Message);
            }
        }

        [TestMethod]
        public void TesterAccesseurPositionEnEcriture ()
        {
            Joueur unJoueur = new Joueur ("Tremblay", "Pierre",
                Position.Centre);

            // Cas 1: Valeur valide.
            unJoueur.Position = Position.AilierGauche;
            Assert.AreEqual ("Tremblay", unJoueur.Nom);
            Assert.AreEqual ("Pierre", unJoueur.Prenom);
            Assert.AreEqual (Position.AilierGauche, unJoueur.Position);

            // Cas 2: Valeur invalide.
            try
            {
                unJoueur.Position = (Position) 6;
                Assert.Fail ();
            }
            catch (ArgumentException ex)
            {
                Assert.AreEqual ("Tremblay", unJoueur.Nom);
                Assert.AreEqual ("Pierre", unJoueur.Prenom);
                Assert.AreEqual (Position.AilierGauche, unJoueur.Position);
                Assert.AreEqual ("La position est invalide.", ex.Message);
            }
        }

        [TestMethod]
        public void TesterMethodeToString ()
        {
            Joueur unJoueur = new Joueur ("Tremblay", "Pierre",
                Position.Centre);
            char [] cLesPositions = new char [] {'C', 'L', 'R', 'D', 'G', 'I'};
            string strInfos;
            int iIndiceDepart;
            int iLongueur;
            int i;

            strInfos = unJoueur.ToString ();
            Assert.AreEqual (72, strInfos.Length);
            Assert.AreEqual ("Tremblay", strInfos.Substring (0,
                strInfos.IndexOf (',')));
            iIndiceDepart = strInfos.IndexOf (',') + 2;
            iLongueur = strInfos.IndexOf (' ', iIndiceDepart) - iIndiceDepart;
            Assert.AreEqual ("Pierre", strInfos.Substring (iIndiceDepart,
                iLongueur));
            Assert.AreEqual (string.Format ("({0})", cLesPositions [0]),
                strInfos.Substring (69, 3));
            for (i = 1; i < cLesPositions.Length; i++)
            {
                unJoueur.Position = (Position) i;
                strInfos = unJoueur.ToString ();
                Assert.AreEqual (string.Format ("({0})", cLesPositions [i]),
                    strInfos.Substring (69, 3));
            }
        }
    }
}
