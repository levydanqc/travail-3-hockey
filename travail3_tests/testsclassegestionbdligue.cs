﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using MySqlConnector;
using Hockey;

namespace travail3_tests
{
    [TestClass]
    public class TestsClasseGestionBDLigue
    {
        [TestMethod]
        public void TesterMethodeObtenirEquipes ()
        {
            MySqlConnection laConnexion;
            MySqlCommand laCommande;
            int iNbEquipes;
            int iNbJoueurs;
            List<Equipe> lesEquipes;
            int iNbJoueursDansEquipes;
            int i;
            int j;
            MySqlDataReader leLecteur;
            List<Equipe> lesBonnesEquipes;
            Joueur leBonJoueur;
            Joueur leJoueur;

            laConnexion = new MySqlConnection ("server=localhost;"
                + "database=14b_travail3;uid=usager;pwd=abcdef");
            laConnexion.Open ();
            laCommande = new MySqlCommand ();
            laCommande.Connection = laConnexion;
            laCommande.CommandText = "select count(*) from equipes;";
            iNbEquipes = Convert.ToInt32 (laCommande.ExecuteScalar ());
            laCommande.CommandText = "select count(*) from joueurs "
                + "where CodeEquipe is not null;";
            iNbJoueurs = Convert.ToInt32 (laCommande.ExecuteScalar ());
            lesEquipes = GestionBDLigue.ObtenirEquipes ();
            Assert.AreEqual (iNbEquipes, lesEquipes.Count);
            iNbJoueursDansEquipes = 0;
            for (i = 0; i < lesEquipes.Count; i++)
            {
                j = 0;
                while (lesEquipes [i].ObtenirJoueur (j) != null)
                {
                    j++;
                    iNbJoueursDansEquipes++;
                }
            }
            Assert.AreEqual (iNbJoueurs, iNbJoueursDansEquipes);
            laCommande.CommandText = "select * from equipes;";
            leLecteur = laCommande.ExecuteReader ();
            lesBonnesEquipes = new List<Equipe> ();
            while (leLecteur.Read ())
            {
                lesBonnesEquipes.Add (new Equipe (leLecteur.GetString ("Code"),
                    leLecteur.GetString ("Nom"),
                    leLecteur.GetString ("Endroit")));
            }
            leLecteur.Close ();
            for (i = 0; i < lesBonnesEquipes.Count; i++)
            {
                Assert.AreEqual (lesBonnesEquipes [i], lesEquipes [i]);
            }
            laCommande.CommandText = "select Nom, Prenom, Position "
                + "from joueurs where CodeEquipe = @CodeEquipe;";
            laCommande.Prepare ();
            laCommande.Parameters.Add ("@CodeEquipe", MySqlDbType.String);
            for (i = 0; i < lesBonnesEquipes.Count; i++)
            {
                laCommande.Parameters ["@CodeEquipe"].Value
                    = lesBonnesEquipes [i].Code;
                leLecteur = laCommande.ExecuteReader ();
                while (leLecteur.Read ())
                {
                    lesBonnesEquipes [i].Ajouter (new Joueur
                        (leLecteur.GetString ("Nom"),
                        leLecteur.GetString ("Prenom"), ObtenirPosition
                        (leLecteur.GetChar ("Position"))));
                }
                leLecteur.Close ();
            }
            for (i = 0; i < lesBonnesEquipes.Count; i++)
            {
                j = 0;
                while ((leBonJoueur = lesBonnesEquipes [i].ObtenirJoueur (j))
                    != null)
                {
                    leJoueur = lesEquipes [i].ObtenirJoueur (j);
                    Assert.AreEqual (leBonJoueur.Nom, leJoueur.Nom);
                    Assert.AreEqual (leBonJoueur.Prenom, leJoueur.Prenom);
                    Assert.AreEqual (leBonJoueur.Position, leJoueur.Position);
                    j++;
                }
            }
            laConnexion.Close ();
        }

        [TestMethod]
        public void TesterMethodeObtenirAgentsLibres ()
        {
            MySqlConnection laConnexion;
            MySqlCommand laCommande;
            int iNbJoueurs;
            List<Joueur> lesAgentsLibres;
            MySqlDataReader leLecteur;
            List<Joueur> lesBonsAgentsLibres;
            int i;
            Joueur leBonJoueur;
            Joueur leJoueur;

            laConnexion = new MySqlConnection ("server=localhost;"
                + "database=14b_travail3;uid=usager;pwd=abcdef");
            laConnexion.Open ();
            laCommande = new MySqlCommand ();
            laCommande.Connection = laConnexion;
            laCommande.CommandText = "select count(*) from joueurs "
                + "where CodeEquipe is null;";
            iNbJoueurs = Convert.ToInt32 (laCommande.ExecuteScalar ());
            lesAgentsLibres = GestionBDLigue.ObtenirAgentsLibres ();
            Assert.AreEqual (iNbJoueurs, lesAgentsLibres.Count);
            laCommande.CommandText = "select Nom, Prenom, Position "
                + "from joueurs where CodeEquipe is null;";
            leLecteur = laCommande.ExecuteReader ();
            lesBonsAgentsLibres = new List<Joueur> ();
            while (leLecteur.Read ())
            {
                lesBonsAgentsLibres.Add (new Joueur
                    (leLecteur.GetString ("Nom"),
                    leLecteur.GetString ("Prenom"),
                    ObtenirPosition (leLecteur.GetChar ("Position"))));
            }
            leLecteur.Close ();
            lesAgentsLibres = GestionBDLigue.ObtenirAgentsLibres ();
            for (i = 0; i < lesBonsAgentsLibres.Count; i++)
            {
                leBonJoueur = lesBonsAgentsLibres [i];
                leJoueur = lesAgentsLibres [i];
                Assert.AreEqual (leBonJoueur.Nom, leJoueur.Nom);
                Assert.AreEqual (leBonJoueur.Prenom, leJoueur.Prenom);
                Assert.AreEqual (leBonJoueur.Position, leJoueur.Position);
            }
            laConnexion.Close ();

        }

        [TestMethod]
        public void TesterMethodeAjouterEquipe ()
        {
            MySqlConnection laConnexion;
            MySqlCommand laCommande;
            MySqlDataReader leLecteur;
            List<string> strLesCodes;
            char [] cLesLettres;
            string strCode;
            Equipe uneEquipe;
            int iNbEquipesAvant;
            int iNbEquipesApres;

            laConnexion = new MySqlConnection ("server=localhost;"
                + "database=14b_travail3;uid=usager;pwd=abcdef");
            laConnexion.Open ();
            laCommande = new MySqlCommand ();
            laCommande.Connection = laConnexion;
            laCommande.CommandText = "select Code from equipes;";
            leLecteur = laCommande.ExecuteReader ();
            strLesCodes = new List<string> ();
            while (leLecteur.Read ())
            {
                strLesCodes.Add (leLecteur.GetString ("Code"));
            }
            leLecteur.Close ();
            laConnexion.Close ();
            cLesLettres = new char [] { 'A', 'A', 'A' };
            strCode = new string (cLesLettres);
            // Note:
            // Il y a 17576 (26 x 26 x 26) codes de trois lettres possibles.
            while (strCode != "[[[" && strLesCodes.Contains (strCode))
            {
                if (cLesLettres [2] < 'Z')
                {
                    cLesLettres [2]++;
                }
                else if (cLesLettres [1] < 'Z')
                {
                    cLesLettres [2] = 'A';
                    cLesLettres [1]++;
                }
                else if (cLesLettres [0] < 'Z')
                {
                    cLesLettres [1] = 'A';
                    cLesLettres [2] = 'A';
                    cLesLettres [0]++;
                }
                else
                {
                    // Note:
                    // Le caractère '[' suit le caractère 'Z' dans la table
                    // ASCII.
                    cLesLettres [0] = '[';
                    cLesLettres [1] = '[';
                    cLesLettres [2] = '[';
                }
                strCode = new string (cLesLettres);
            }
            if (strCode != "[[[")
            {
                uneEquipe = new Equipe (strCode, "Nordiques", "Quebec");
                laConnexion.Open ();
                laCommande.CommandText = "select count(*) from equipes;";
                iNbEquipesAvant = Convert.ToInt32 (laCommande
                    .ExecuteScalar ());
                try
                {
                    GestionBDLigue.AjouterEquipe (uneEquipe);
                    Assert.Fail ();
                }
                catch (ArgumentOutOfRangeException)
                {
                    iNbEquipesApres = Convert.ToInt32 (laCommande
                        .ExecuteScalar ());
                    Assert.AreEqual (iNbEquipesAvant,
                        iNbEquipesApres);
                }
                GestionBDLigue.PreparerRequetes ();
                GestionBDLigue.AjouterEquipe (uneEquipe);
                iNbEquipesApres = Convert.ToInt32 (laCommande
                    .ExecuteScalar ());
                Assert.AreEqual (iNbEquipesAvant + 1, iNbEquipesApres);
                iNbEquipesAvant = iNbEquipesApres;
                try
                {
                    GestionBDLigue.AjouterEquipe (uneEquipe);
                    Assert.Fail ();
                }
                catch (MySqlException)
                {
                    iNbEquipesApres = Convert.ToInt32 (laCommande
                        .ExecuteScalar ());
                    Assert.AreEqual (iNbEquipesAvant, iNbEquipesApres);
                }
                GestionBDLigue.LibererRequetes ();
                laCommande.CommandText = "delete from equipes "
                    + "where Code = @Code;";
                laCommande.Parameters.AddWithValue ("@Code", strCode);
                laCommande.ExecuteNonQuery ();
                laConnexion.Close ();
            }
            else
            {
                throw new InternalTestFailureException ("Imposible "
                    + "d'effectuer le test car La table equipes contient déjà "
                    + "toutes les valeurs possibles de la clé primaire.");
            }
        }

        [TestMethod]
        public void TesterMethodeSupprimerEquipe ()
        {
            MySqlConnection laConnexion;
            MySqlCommand laCommande;
            MySqlDataReader leLecteur;
            Equipe uneEquipe;
            List <int> iLesNumeros;
            int iNbEquipesAvant;
            int iNbEquipesApres;
            int i;

            laConnexion = new MySqlConnection ("server=localhost;"
                + "database=14b_travail3;uid=usager;pwd=abcdef");
            laConnexion.Open ();
            laCommande = new MySqlCommand ();
            laCommande.Connection = laConnexion;
            laCommande.CommandText = "select * from equipes order by rand() "
                + "limit 1;";
            leLecteur = laCommande.ExecuteReader ();
            leLecteur.Read ();
            uneEquipe = new Equipe (leLecteur.GetString ("Code"),
                leLecteur.GetString ("Nom"), leLecteur.GetString ("Endroit"));
            leLecteur.Close ();
            laCommande.CommandText = "select Numero from joueurs "
                + "where CodeEquipe = @CodeEquipe;";
            laCommande.Parameters.AddWithValue ("@CodeEquipe", uneEquipe.Code);
            leLecteur = laCommande.ExecuteReader ();
            iLesNumeros = new List <int> ();
            while (leLecteur.Read ())
            {
                iLesNumeros.Add (leLecteur.GetInt32 ("Numero"));
            }
            leLecteur.Close ();
            laCommande.CommandText = "Select count(*) from equipes;";
            iNbEquipesAvant = Convert.ToInt32 (laCommande.ExecuteScalar ());
            try
            {
                GestionBDLigue.SupprimerEquipe (uneEquipe);
                Assert.Fail ();
            }
            catch (ArgumentOutOfRangeException)
            {
                iNbEquipesApres = Convert.ToInt32 (laCommande
                    .ExecuteScalar ());
                Assert.AreEqual (iNbEquipesAvant, iNbEquipesApres);
            }
            GestionBDLigue.PreparerRequetes ();
            GestionBDLigue.SupprimerEquipe (uneEquipe);
            iNbEquipesApres = Convert.ToInt32 (laCommande.ExecuteScalar ());
            Assert.AreEqual (iNbEquipesAvant - 1, iNbEquipesApres);
            iNbEquipesAvant = iNbEquipesApres;
            GestionBDLigue.SupprimerEquipe (uneEquipe);
            iNbEquipesApres = Convert.ToInt32 (laCommande.ExecuteScalar ());
            Assert.AreEqual (iNbEquipesAvant, iNbEquipesApres);
            GestionBDLigue.LibererRequetes ();
            laCommande.CommandText = "insert into equipes values (@Code, "
                + "@Nom, @Endroit);";
            laCommande.Parameters.Clear ();
            laCommande.Parameters.AddWithValue ("@Code", uneEquipe.Code);
            laCommande.Parameters.AddWithValue ("@Nom", uneEquipe.Nom);
            laCommande.Parameters.AddWithValue ("@Endroit", uneEquipe.Endroit);
            laCommande.ExecuteNonQuery ();
            laCommande.CommandText = "update joueurs "
                + "set CodeEquipe = @CodeEquipe where Numero = @Numero;";
            laCommande.Prepare ();
            laCommande.Parameters.Clear ();
            laCommande.Parameters.Add ("@Numero", MySqlDbType.Int32);
            laCommande.Parameters.AddWithValue ("@CodeEquipe", uneEquipe.Code);
            for (i = 0; i < iLesNumeros.Count; i++)
            {
                laCommande.Parameters ["@Numero"].Value = iLesNumeros [i];
                laCommande.ExecuteNonQuery ();
            }
            laConnexion.Close ();
        }

        [TestMethod]
        public void TesterMethodeAjouterJoueur ()
        {
            MySqlConnection laConnexion;
            MySqlCommand laCommande;
            Joueur unJoueur;
            int iNbJoueursAvant;
            int iNbJoueursApres;
            Position [] lesPositions;
            int i;
            MySqlDataReader leLecteur;

            laConnexion = new MySqlConnection ("server=localhost;"
                + "database=14b_travail3;uid=usager;pwd=abcdef");
            laConnexion.Open ();
            laCommande = new MySqlCommand ();
            laCommande.Connection = laConnexion;
            laCommande.CommandText = "select count(*) from joueurs;";
            unJoueur = new Joueur ("Tremblay", "Pierre", Position.Centre);
            iNbJoueursAvant = Convert.ToInt32 (laCommande.ExecuteScalar ());
            try
            {
                GestionBDLigue.AjouterJoueur (unJoueur);
                Assert.Fail ();
            }
            catch (ArgumentOutOfRangeException)
            {
                iNbJoueursApres = Convert.ToInt32 (laCommande
                    .ExecuteScalar ());
                Assert.AreEqual (iNbJoueursAvant, iNbJoueursApres);
            }

            lesPositions = (Position []) Enum.GetValues (typeof (Position));
            GestionBDLigue.PreparerRequetes ();
            GestionBDLigue.AjouterJoueur (unJoueur);
            for (i = 1; i < lesPositions.Length; i++)
            {
                unJoueur.Position = lesPositions [i];
                GestionBDLigue.AjouterJoueur (unJoueur);
            }
            GestionBDLigue.LibererRequetes ();

            iNbJoueursApres = Convert.ToInt32 (laCommande.ExecuteScalar ());
            Assert.AreEqual (iNbJoueursAvant + lesPositions.Length,
                iNbJoueursApres);

            laCommande.CommandText = "select Nom, Prenom, Position, CodeEquipe "
                + "from joueurs order by Numero desc "
                + "limit @NbEnregistrements;";
            laCommande.Parameters.AddWithValue ("@NbEnregistrements",
                lesPositions.Length);
            leLecteur = laCommande.ExecuteReader ();
            i = lesPositions.Length - 1;
            while (leLecteur.Read ())
            {
                Assert.AreEqual (unJoueur.Nom, leLecteur.GetString ("Nom"));
                Assert.AreEqual (unJoueur.Prenom, leLecteur
                    .GetString ("Prenom"));
                Assert.AreEqual (lesPositions [i], ObtenirPosition
                    (leLecteur.GetChar ("Position")));
                Assert.IsTrue (leLecteur.IsDBNull (leLecteur.GetOrdinal
                    ("CodeEquipe")));
                i--;
            }
            leLecteur.Close ();
            laCommande.CommandText = "delete from joueurs order by Numero "
                + "desc limit @NbEnregistrements;";
            laCommande.ExecuteNonQuery ();
            laConnexion.Close ();
        }

        [TestMethod]
        public void TesterMethodeAffecterJoueurAEquipe ()
        {
            MySqlConnection laConnexion;
            MySqlCommand laCommande;
            MySqlDataReader leLecteur;
            Equipe uneEquipe;
            int iNumero;
            Joueur unJoueur;
            int iNbJoueurs;

            laConnexion = new MySqlConnection ("server=localhost;"
                + "database=14b_travail3;uid=usager;pwd=abcdef");
            laConnexion.Open ();
            laCommande = new MySqlCommand ();
            laCommande.Connection = laConnexion;
            laCommande.CommandText = "select * from equipes order by rand() "
                + "limit 1;";
            leLecteur = laCommande.ExecuteReader ();
            leLecteur.Read ();
            uneEquipe = new Equipe (leLecteur.GetString ("Code"),
                leLecteur.GetString ("Nom"), leLecteur.GetString ("Endroit"));
            leLecteur.Close ();
            laCommande.CommandText = "select * from joueurs "
                + "where CodeEquipe is null order by rand() limit 1;";
            leLecteur = laCommande.ExecuteReader ();
            leLecteur.Read ();
            iNumero = leLecteur.GetInt32 ("Numero");
            unJoueur = new Joueur (leLecteur.GetString ("Nom"),
                leLecteur.GetString ("Prenom"),
                ObtenirPosition (leLecteur.GetChar ("Position")));
            leLecteur.Close ();
            laCommande.CommandText = "select count(*) from joueurs "
                + "where CodeEquipe = @CodeEquipe;";
            laCommande.Parameters.AddWithValue ("@CodeEquipe", uneEquipe.Code);
            iNbJoueurs = Convert.ToInt32 (laCommande.ExecuteScalar ());
            try
            {
                GestionBDLigue.AffecterJoueurAEquipe (uneEquipe, unJoueur);
                Assert.Fail ();
            }
            catch (ArgumentOutOfRangeException)
            {
                Assert.AreEqual (iNbJoueurs, Convert.ToInt32
                    (laCommande.ExecuteScalar ()));
                laCommande.CommandText = "select CodeEquipe from joueurs "
                    + "where Numero = @Numero;";
                laCommande.Parameters.AddWithValue ("@Numero", iNumero);
                Assert.AreEqual (DBNull.Value, laCommande.ExecuteScalar ());

            }
            GestionBDLigue.PreparerRequetes ();
            GestionBDLigue.AffecterJoueurAEquipe (uneEquipe, unJoueur);
            GestionBDLigue.LibererRequetes ();
            laCommande.CommandText = "select * from joueurs "
                + "where Numero = @Numero;";
            leLecteur = laCommande.ExecuteReader ();
            leLecteur.Read ();
            Assert.AreEqual (unJoueur.Nom, leLecteur.GetString ("Nom"));
            Assert.AreEqual (unJoueur.Prenom, leLecteur.GetString ("Prenom"));
            Assert.AreEqual (unJoueur.Position,
                ObtenirPosition (leLecteur.GetChar ("Position")));
            leLecteur.Close ();
            laCommande.CommandText = "select count(*) from joueurs "
                + "where CodeEquipe = @CodeEquipe;";
            Assert.AreEqual (iNbJoueurs + 1, Convert.ToInt32
                (laCommande.ExecuteScalar ()));
            laCommande.CommandText = "update joueurs set CodeEquipe = null "
                + "where Numero = @Numero;";
            laCommande.ExecuteNonQuery ();
            laConnexion.Close ();
        }

        private Position ObtenirPosition (char cPosition)
        {
            switch (cPosition)
            {
                case 'C':
                    return Position.Centre;
                case 'L':
                    return Position.AilierGauche;
                case 'R':
                    return Position.AilierDroit;
                case 'D':
                    return Position.Defenseur;
                case 'G':
                    return Position.GardienDeBut;
                default:
                    return Position.ADeterminer;
            }
        }
    }
}
